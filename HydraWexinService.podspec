Pod::Spec.new do |s|
  s.name         = "HydraWexinService"
  s.version      = "0.0.10"
  s.summary      = "HydraWexinService release."
  s.description  = <<-DESC
                   HydraWexinService release, support share/pay.
                   DESC
  s.homepage     = "https://bitbucket.org/hydra-service/hydrawexinservice"
  s.license      = "MIT"
  s.author       = { "samchang" => "sam.chang@me.com" }
  s.platform     = :ios, "6.0.0"
  s.source       = { :git => "https://bitbucket.org/hydra-service/hydrawexinservice.git", :tag => "v0.0.10" }

  s.default_subspec = 'Release'

  s.subspec 'Release' do |ss|
    ss.source_files  = "*.{h,m}"
    ss.dependency 'Hydra32/Release'
    ss.xcconfig =  {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Hydra32/Release'}
  end

  s.subspec 'Debug' do |ss|
    ss.source_files  = "*.{h,m}"
    ss.dependency 'Hydra32/Debug'
    ss.xcconfig =  {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Hydra32/Debug'}
  end

  s.subspec 'Release33' do |ss|
    ss.source_files  = "*.{h,m}"
    ss.dependency 'Hydra33/Release'
    ss.xcconfig =  {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Hydra33/Release'}
  end

  s.subspec 'Debug33' do |ss|
    ss.source_files  = "*.{h,m}"
    ss.dependency 'Hydra33/Debug'
    ss.xcconfig =  {'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Hydra33/Debug'}
  end

  s.ios.vendored_libraries = '*.a'
end
