//
//  ShareWeixinService.h
//  HydraShareService
//
//  Created by Sam Chang on 3/13/15.
//  Copyright (c) 2015 Hydra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EOS/IService.h>
#import <EOS/AbstractLuaTableCompatible.h>
#import <WXApi.h>

@interface WeixinService : AbstractLuaTableCompatible <IService, WXApiDelegate> {
    lua_State *L;
}

@property (nonatomic, readonly) NSDictionary *args;

- (BOOL) canHandleResponse;

@end
