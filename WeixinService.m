//
//  ShareWeixinService.m
//  HydraShareService
//
//  Created by Sam Chang on 3/13/15.
//  Copyright (c) 2015 Hydra. All rights reserved.
//

#import "WeixinService.h"
#import <EOS/ESRegistry.h>
#import <WXApi.h>
#import <EOS/OSUtils.h>
#import <EOS/EOSAppDelegate.h>

@implementation WeixinService

-(BOOL)singleton{
    return YES;
}

+(void)load{
    [[ESRegistry getInstance] registerService: @"WeixinService" withName: @"weixin"];
}

- (BOOL) canHandleResponse{
    return _args != nil;
}

-(void)onReq:(BaseReq *)req{
}

-(void)onResp:(BaseResp *)resp{
    LuaFunction *onresp = [_args valueForKey: @"onResp"];
    if ([onresp isKindOfClass: [LuaFunction class]]) {
        NSMutableDictionary *map = [NSMutableDictionary dictionary];
        [map setValue: [NSNumber numberWithInt: resp.errCode] forKey: @"errCode"];
        [map setValue: resp.errStr forKey: @"errStr"];
        [map setValue: [NSNumber numberWithInt: resp.type] forKey: @"type"];
        if ([resp isKindOfClass: [PayResp class]]) {
            [map setValue: ((PayResp *) resp).returnKey forKey: @"returnKey"];
        } else if ([resp isKindOfClass: [SendMessageToWXResp class]]) {
            [map setValue: ((SendMessageToWXResp *) resp).country forKey: @"country"];
            [map setValue: ((SendMessageToWXResp *) resp).lang forKey: @"lang"];
        } else if ([resp isKindOfClass: [SendAuthResp class]]) {
            [map setValue: ((SendAuthResp *) resp).code forKey: @"code"];
            [map setValue: ((SendAuthResp *) resp).state forKey: @"state"];
        }

        [onresp executeWithoutReturnValue: map, nil];
    }

    _args = nil;
}

-(void)setCurrentState:(lua_State *)L2{
    L = L2;
}

- (BOOL) share: (NSDictionary *) args{
    if (![args isKindOfClass: [NSDictionary class]]) {
        NSLog(@"invaild args on weixin share");
        return NO;
    }
    NSString *kind = [args valueForKey: @"kind"];

    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    if ([kind isEqual: @"text"]) {
        NSString *text = [args valueForKey: @"text"];
        if ([text isKindOfClass: [NSString class]]) {
            req.text = text;

            if ([text length] > 10 * 1024) {
                NSLog(@"Warning: text size over 10240, share may fail.");
            }

            req.bText = YES;
        } else {
            NSLog(@"incorrect text on shareweixin");
            return NO;
        }
    } else if ([kind isEqual: @"media"]) {
        req.bText = NO;
        WXMediaMessage *msg = [WXMediaMessage message];
        NSString *mediaType = [args valueForKey: @"mediatype"];
        NSString *url = [args valueForKey: @"url"];

        if (![url isKindOfClass: [NSString class]]) {
            NSLog(@"incorrect url on shareweixin");
            return NO;
        }

        msg.title = [args valueForKey: @"title"];
        msg.description = [args valueForKey: @"description"];
        msg.mediaTagName = [args valueForKey: @"mediaTagName"];
        msg.messageExt = [args valueForKey: @"messageExt"];
        msg.messageAction = [args valueForKey: @"messageAction"];

        AppSandbox *sandbox = [OSUtils getSandboxFromState: L];

        if ([args valueForKey: @"thumbnail"]) {
            msg.thumbData = [NSData dataWithContentsOfFile:
                             [[sandbox resolveFile: [args valueForKey: @"thumbnail"]] path]];

            if ([msg.thumbData length] > 32 * 1024) {
                NSLog(@"Warning: Thumbnail size over 32768, share may fail.");
            }
        }

        if ([mediaType isEqual: @"image"]) {
            WXImageObject *obj = [WXImageObject object];
            if ([url hasPrefix: @"http"]) {
                obj.imageUrl = url;
            } else {
                NSURL *uri = [sandbox resolveFile: url];
                obj.imageData = [NSData dataWithContentsOfFile: [uri path]];
            }
            msg.mediaObject = obj;
        } else if ([mediaType isEqual: @"emoticon"]) {
            WXEmoticonObject *obj = [WXEmoticonObject object];

            NSURL *uri = [sandbox resolveFile: url];
            obj.emoticonData = [NSData dataWithContentsOfFile: [uri path]];
            msg.mediaObject = obj;
        } else if ([mediaType isEqual: @"webpage"]) {
            WXWebpageObject *obj = [WXWebpageObject object];
            obj.webpageUrl = url;
            msg.mediaObject = obj;
        }

        req.message = msg;
    } else {
        NSLog(@"incorrect type on shareweixin");
        return NO;
    }

    NSString *scene = [args valueForKey: @"scene"];

    if ([scene isEqual: @"session"]) {
        req.scene = WXSceneSession;
    } else if ([scene isEqual: @"timeline"]) {
        req.scene = WXSceneTimeline;
    } else if ([scene isEqual: @"favorite"]) {
        req.scene = WXSceneFavorite;
    }

    _args = args;

    return [WXApi sendReq: req];
}

- (BOOL) auth: (NSDictionary *) args {
    SendAuthReq *req = [[SendAuthReq alloc] init];

    req.state = [args valueForKey: @"state"];

    NSString *scope = [args valueForKey: @"scope"];
    if (![scope isKindOfClass: [NSString class]]) {
        scope = @"snsapi_userinfo";
    }
    req.scope = scope;

    _args = args;

    UIViewController *rootViewController = ((EOSAppDelegate *) [UIApplication sharedApplication].delegate).rootViewController;
    return [WXApi sendAuthReq: req viewController: rootViewController delegate: self];
}

- (BOOL) pay: (NSDictionary *) args{
    if (![args isKindOfClass: [NSDictionary class]]) {
        NSLog(@"invaild args on weixin pay");
        return NO;
    }

    PayReq* req = [[PayReq alloc] init];
    req.package = [args valueForKey: @"package"];
    req.nonceStr = [args valueForKey: @"nonceStr"];
    req.partnerId = [args valueForKey: @"partnerId"];
    req.prepayId = [args valueForKey: @"prepayId"];
    req.sign = [args valueForKey: @"sign"];
    req.timeStamp = [[args valueForKey: @"timeStamp"] intValue];

    _args = args;
    return [WXApi sendReq:req];
}

- (BOOL) getInstalled {
    return [WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi];
}

@end
